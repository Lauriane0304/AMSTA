using UnityEngine;
using System.Collections;
using System;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


// This class handle the behaviour of the UI for the CheckList scene
public class CheckListManager : MonoBehaviour
{

    public Button myButtonStartProc;
    public Button myButtonBack;

    public Toggle myToggleComponent1;
    public Toggle myToggleComponent2;

    private bool isToggle1ON = false;
    private bool isToggle2ON = false;
    


    public void Start()
    {
        Screen.orientation = ScreenOrientation.LandscapeLeft;

       
    	myButtonStartProc.onClick.AddListener(TaskOnClick);
		myButtonBack.onClick.AddListener(TaskOnClick);

        myButtonStartProc.gameObject.SetActive(false);

        myToggleComponent1.onValueChanged.AddListener((value) =>
        {
                MyListenerToggle1(value);
        });

        myToggleComponent2.onValueChanged.AddListener((value) =>
        {
                MyListenerToggle2(value);
        });

       

    }

    public void MyListenerToggle1(bool value)
    {
        if(value)
        {
            isToggle1ON = true;
        }
        else 
        {
            isToggle1ON = false;
        }

    }

    public void MyListenerToggle2(bool value)
    {
        if(value)
        {
            isToggle2ON = true;
        }
        else 
        {
            isToggle2ON = false;
        }

    }



    void ChangeScene(string sceneName)
	{
	    SceneManager.LoadScene (sceneName);
	}

    // The button "start the procedure" is enabled only if the toggles are checked
    void Update()
    {
        if(isToggle1ON && isToggle2ON)
        {
            myButtonStartProc.gameObject.SetActive(true);
        }
        else
        {
            myButtonStartProc.gameObject.SetActive(false);
        }
       
    }


    void TaskOnClick()
    {
        if(EventSystem.current.currentSelectedGameObject.name == "ButtonBack")
        {
            Debug.Log("Button Back CheckList Pressed !");

            ChangeScene("MainScene");
        }
        
        else if(EventSystem.current.currentSelectedGameObject.name == "ButtonStartProc")
        {
            Debug.Log("Button Start Proc CheckList Pressed !");

            ChangeScene("SceneEmptyShell");
        }

    }
}