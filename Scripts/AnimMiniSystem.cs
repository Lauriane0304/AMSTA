using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// This class handle the graphic behaviour of the system on the screen
public class AnimMiniSystem : MonoBehaviour {

	public float amplitudeY;
	public float omegaY;
	public float myAnglePerFrame;

	

	private float index;
	private float tmpAngle;
	private float angleY = 0;

	private Vector3 initialPosition;
	private Vector3 initialRotation;

	// Use this for initialization
	void Start () {

		index = 0;
		initialPosition = transform.localPosition + new Vector3(0,0.04f,0);
		initialRotation = transform.localRotation.eulerAngles;
		tmpAngle = myAnglePerFrame;
	}
	
	// Update is called once per frame
	void Update () {

		angleY += myAnglePerFrame;
		if(angleY>=360)
			angleY = 0;
		transform.localRotation = Quaternion.Euler(initialRotation +new Vector3(0,angleY,0));

		index += Time.deltaTime;
		float y = (amplitudeY*Mathf.Sin (omegaY*index));
		transform.localPosition= new Vector3(0,y,0) + initialPosition;
	}

	// Stop the rotation of the system on finger down
	void OnMouseDown()
    {
        myAnglePerFrame = 0;
    }

    // Restart the rotation of the system on finger up
    void OnMouseUp()
    {
        myAnglePerFrame = tmpAngle;
    }

}
