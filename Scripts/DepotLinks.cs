// This class contains the URLs for the bundles in order to download them in the SceneEmptyShell
public class DepotLinks  
{
	static public string versionLink = "";    	// The version bundle
	static public string mainLink = "";			// The asset bundle
}