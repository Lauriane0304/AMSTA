using UnityEngine;
using System.Collections;
using Vuforia; 
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

// This class manages the information messages which is displayed to the user
public class OnTargetAquired : MonoBehaviour, ITrackableEventHandler {
    
    //Variable of AssetBundle Manager type allowing to use AssetBundles more streamlined.
    public MyAssetBundleManager myABM;
    
    //Boolean to show the state of the target (Aquired or not)
    public bool Aquired = false;
    public bool TargetActivated = false;

    //Banner and aquired label images
    public UnityEngine.UI.Image myAquiredLabel;
    public UnityEngine.UI.Image myBannerLoad;
    public UnityEngine.UI.Image myBannerScan;

    //Sprites are 2D graphic objects used for characters, props, projectiles and other elments of 2D gameplay
    public Sprite textureLoadingToDisplay;
    public Sprite textureProblemDownloadingToDisplay;
    public Sprite textureScanTargetToDisplay;
    public Sprite textureTargetAquiredToDisplay;
    public Sprite textureWarningDownloadingToDisplay;



    //Vuforia variable to augmentation objects
    private TrackableBehaviour mTrackableBehaviour;

    //Booleans allowing to show the state ??
    private bool mShowGUIButton = false;
    private bool ShowlLabel2Sec = true;
     

    /**Start is called on the frame when a script is enabled 
    /* just before any of the Update methods is called the first time.
    */
    public void Start () 
    {
        mTrackableBehaviour = GetComponent<TrackableBehaviour>();
        
        //Registers a new Tracker event handler at the Tracker. 
        //These handlers are called as soon as ALL Trackables have been updated in this frame.
        if (mTrackableBehaviour)
            mTrackableBehaviour.RegisterTrackableEventHandler(this);

        //Disabled the aquired label as well as the scan banner.
        //Enabled the loading banner
        myAquiredLabel.enabled = false;
        myAquiredLabel.sprite = textureTargetAquiredToDisplay;
        myBannerLoad.enabled = true;
        myBannerLoad.sprite = textureLoadingToDisplay;
        myBannerScan.enabled = false;

        //Desactivate all the child of the current image target
        myABM.activateMyGameObject(false);

        //Add buttons to the listener TaskOnClick
        myABM.yesButton.onClick.AddListener(TaskOnClick);
        myABM.noButton.onClick.AddListener(TaskOnClick);
    }

    
    /** Called when the trackable state has changed.
    */
    public void OnTrackableStateChanged(
                                    TrackableBehaviour.Status previousStatus,
                                    TrackableBehaviour.Status newStatus)
    {
        //If the trackableResult was detected or tracked or tracked while not being visible
        if (newStatus == TrackableBehaviour.Status.DETECTED ||
            newStatus == TrackableBehaviour.Status.TRACKED ||
            newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
        {
            //If the asset bundle is instanciate and the trackable object is the same that the current image target 
            //and the menu mode is not activated, then all the child of the current image target are activated
            if (myABM.BundleReady && mTrackableBehaviour.TrackableName == myABM.myCurrentImageTarget.name && !myABM.isInfoCanvasDisplayed)
                myABM.activateMyGameObject(true);

            //The target is read, so the aquired label can be shown
            mShowGUIButton = true;
            Aquired = true;
            Invoke("ToggleLabel", 2);
        }

        else
        {
            mShowGUIButton = false;
            Aquired = false;
        }
    }

    /** Function to wait 2 seconds before putting mShowGUIButton to false and 
    /* therefore undisplay the label aquired on the screen 
    */
    public void ToggleLabel() 
    {
        ShowlLabel2Sec = !ShowlLabel2Sec;
        mShowGUIButton = false;
    }

    /** Function to wait 3 seconds before disabled myBannerLoad
    */
    public void Wait() {
        myBannerLoad.enabled = false;
    }

    /** Listener allowing to react to the user actions 
    */
    void TaskOnClick() {
        
        if (EventSystem.current.currentSelectedGameObject.name == "YesButton") {
            //Suppress all instantiate gameobjects and unload the bundle
            myABM.CloseAll();

            //Load the mainScene
            SceneManager.LoadScene("MainScene");
        }
        else if (EventSystem.current.currentSelectedGameObject.name == "NoButton") {
            //Disactivate the backbutton
            myABM.backButton.SetActive(false);
        } 
    }
     
    /** Update is called every frame, if the MonoBehaviour is enabled.
    */
    void Update()
    {
        Debug.Log("OnTargetAquired : " + myABM.ProblemDownloading);
        //If the back button of the tablet/smartphone is pressed, display the backbutton
        if (Input.GetKeyDown(KeyCode.Escape))
            myABM.backButton.SetActive(true);

        //If there is a problem downloading the bundle, put the right texture
        if (myABM.ProblemDownloading) 
            myBannerLoad.sprite = textureProblemDownloadingToDisplay;
        
        //If there is no internet, put the right texture
        else if (myABM.WarningDownloading)
            myBannerLoad.sprite = textureWarningDownloadingToDisplay;

        else 
            myBannerLoad.sprite = textureLoadingToDisplay;

        //If the target is read
        if(TargetActivated)
        {
            if (myABM.WarningDownloading)
                Invoke("Wait", 3);

            else
                myBannerLoad.enabled = false;

            //If the target has been read and we didn't display the aquired label yet 
            if (mShowGUIButton && ShowlLabel2Sec)
                myAquiredLabel.enabled = true;

            else
                myAquiredLabel.enabled = false;

            //If the bundle is ready and the target is not read
            if (myABM.BundleReady && !Aquired)
            {
                //Enable the loading banner
                myBannerScan.sprite = textureScanTargetToDisplay;
                myBannerScan.enabled = true;
            }
            else
            {
                myBannerScan.enabled = false;

                //If we are not in the info mode, activate all the gameobjects of the scene
                if(!myABM.isInfoCanvasDisplayed)
                    myABM.activateMyGameObject(true);
            }
        }   
    }
}
