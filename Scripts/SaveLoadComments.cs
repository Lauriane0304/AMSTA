﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

// This class allows to load, save and delete the comments
public static class SaveLoadComments {

	public static List<CommentClass> savedComments = new List<CommentClass>();
	public static string fileName;

	public static void Load()
	{
		if(File.Exists(Application.persistentDataPath + "/" + fileName + ".jg"))
		{
	        BinaryFormatter bf = new BinaryFormatter();
	        FileStream file = File.Open(Application.persistentDataPath + "/" + fileName + ".jg", FileMode.Open);
	        SaveLoadComments.savedComments = (List<CommentClass>)bf.Deserialize(file);
	        file.Close();
	    }
	}


	public static void Save()
	{
		savedComments.Add(CommentClass.currentComment);
	    BinaryFormatter bf = new BinaryFormatter();
	    FileStream file = File.Create (Application.persistentDataPath + "/" + fileName + ".jg");
	    bf.Serialize(file, SaveLoadComments.savedComments);
	    file.Close();
	}

	public static void Delete()
	{
		for(int i=0; i<savedComments.Count; i++)
		{
			if(savedComments[i].title.Equals(CommentClass.currentComment.title) && savedComments[i].step==CommentClass.currentComment.step)
			{
				savedComments.RemoveAt(i);
				break;
			}
		}

		//Delete screenshot
		if(File.Exists(@CommentClass.currentComment.linkVideo))
		{
			Debug.Log("Screenshot trouvé !");
		    File.Delete(@CommentClass.currentComment.linkVideo);
		    Debug.Log("Screenshot Supprimée !");
		}

		BinaryFormatter bf = new BinaryFormatter();
	    FileStream file = File.Create (Application.persistentDataPath + "/" + fileName + ".jg");
	    bf.Serialize(file, SaveLoadComments.savedComments);
	    file.Close();
	}
}
