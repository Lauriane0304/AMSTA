﻿[System.Serializable]
public class JSONSensorData
{
	public string title;
	public float value;
}

[System.Serializable]
public class JSONSensor
{
	public string name;
	public JSONSensorData[] data;
}
 

[System.Serializable]
public class JSONSensors
{
   public JSONSensor[] sensors;
}
