// A Prefab is defined by its EXACT name, beware of the name in the json file
[System.Serializable]
public class JSONPrefab
{
	public string name;
	public int sceneNumber;
	public int target;
} 

// A Scene is defined by its target number (1 --> 6, 0 when there's no Image Target)
[System.Serializable]
public class JSONScene
{
	public string name;
	public int sceneNumber;
	public JSONSprite[] sprites;
	public int target;
} 

// A Sprite is defined by its EXACT name and extension (jpg, png)
[System.Serializable]
public class JSONSprite
{
   public string name;
}


[System.Serializable]
public class JSONBanner
{
	public int targetNumber;
	public string spriteName;
}

// An Index is the main container called in the AssetBundleManager
[System.Serializable]
public class JSONIndex
{
   public JSONPrefab[] prefabs;	
   public JSONScene[] scenes;
   public string title;
   public string commentsFileName;
   public JSONBanner[] banners;
}