﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

//This class close the canvas on button pressed
public class BackCanvasOperational : MonoBehaviour
{
    public Button myButton;
    public Canvas myCanvas;
    public GameObject myImageTarget;

    void Start()
    {
        Button btn = myButton.GetComponent<Button>();
        btn.onClick.AddListener(TaskOnClick);
    }

    void TaskOnClick()
    {
        myCanvas.enabled = false;
        myImageTarget.transform.GetChild(0).gameObject.SetActive(true);
        myImageTarget.transform.GetChild(1).gameObject.SetActive(true);
    }
}