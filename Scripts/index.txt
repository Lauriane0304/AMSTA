{
  "prefabs": [
    {
      "name": "cubeRed",
      "sceneNumber": "1",
      "positionX": "-0.5f",
      "positionY": "0.5f",
      "positionZ": "0.5f",
      "rotationX": "-180.0f",
      "rotationY": "0.0f",
      "rotationZ": "0.0f",
      "scaleX": "11.76f",
      "scaleY": "11.23f",
      "scaleZ": "11.23f"
    },
    {
      "name": "cubeGreen",
      "sceneNumber": "1",
      "positionX": "-0.5f",
      "positionY": "0.0f",
      "positionZ": "0.5f",
      "rotationX": "-180.0f",
      "rotationY": "0.0f",
      "rotationZ": "0.0f",
      "scaleX": "19.04f",
      "scaleY": "19.04f",
      "scaleZ": "19.04f"
    },
    {
      "name": "cubePurple",
      "sceneNumber": "2",
      "positionX": "-0.5f",
      "positionY": "0.0f",
      "positionZ": "0.5f",
      "rotationX": "0.0f",
      "rotationY": "0.0f",
      "rotationZ": "0.0f",
      "scaleX": "24.95f",
      "scaleY": "24.95f",
      "scaleZ": "24.95f"
    }
  ]
}