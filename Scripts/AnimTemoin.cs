using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// This class handle the graphic behaviour of ROI on the system on the screen
public class AnimTemoin : MonoBehaviour {

	public float amplitude = 10f;
	public float omega = 7f;
	

	private float index;
	private Vector3 initialScale;

	// Use this for initialization
	void Start () {

		index = 0;
		initialScale = transform.localScale;
	}
	
	// Update is called once per frame
	void Update () {

		index += Time.deltaTime;
		float delta = (amplitude*Mathf.Sin (omega*index));
		transform.localScale= new Vector3(delta,delta,delta) + initialScale;
	}

}
