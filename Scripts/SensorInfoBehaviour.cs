﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using Vuforia;


// This class handle the behaviour of the arrows on the operationnal mode
public class SensorInfoBehaviour : MonoBehaviour
{
    public Canvas myCanvas;
    public string text;
    public Text myTextSensor;
    public Text myTextSupplier;
    public Text myTextTechnology;
    public Text myTextPrinciple;

    public string myStringSensor;
    public string myStringSupplier;
    public string myStringTechnology;
    public string myStringPrinciple;

    public Sprite mySensorSprite;
    public Sprite myCompanySprite;

    public UnityEngine.UI.Image mySensorImage;
    public UnityEngine.UI.Image myCompanyImage; 

    public GameObject myImageTarget;

    public GUIStyle guiStyle = null;



    #region PRIVATE_MEMBER_VARIABLES

    private bool pressed = false;

    // State 1 is when sensor values are displayed
    // State 2 is when sensor data sheet is displayed
    // State 0 nothing is displayed
    private int state = 0;

    private bool valuesDisplayed = true;
    private float temp1;
    
    private string arrowName;

    #endregion // PRIVATE_MEMBER_VARIABLES



    #region UNTIY_MONOBEHAVIOUR_METHODS

    public void Start()
    {        
        myCanvas.enabled = false;   
    }

    
    public void Update()
    {
        if(pressed)
        {
            // If press time > 0.5s, display technical sheet of the sensor
            if((Time.time - temp1) > 0.5)
            {
                state = 2;
                
                myTextSensor.text = myStringSensor;
                myTextSupplier.text = myStringSupplier;
                myTextTechnology.text = myStringTechnology;
                myTextPrinciple.text = myStringPrinciple;

                mySensorImage.sprite = mySensorSprite;
                myCompanyImage.sprite = myCompanySprite;

                myCanvas.enabled = true;
                myImageTarget.transform.GetChild(0).gameObject.SetActive(false);
                myImageTarget.transform.GetChild(1).gameObject.SetActive(false);
                pressed = false;
            }

            // Else, show the value of the sensor on the arrow
            else
            {
                state = 1;
            }
        }
    }

    void OnMouseDown()
    {
        pressed = true;
        temp1 = Time.time;
    }

    void OnMouseUp()
    {
        pressed = false;
        if(state==1)
            valuesDisplayed = !valuesDisplayed;
    }
    
    public void OnGUI()
    {
        if(!valuesDisplayed && state==1)
        {
            var position = Camera.main.WorldToScreenPoint(this.transform.position);
            var textSize = GUI.skin.label.CalcSize(new GUIContent(text));
            // TODO - Adapt the position
            GUI.Label(new Rect(position.x, Screen.height - position.y, textSize.x, textSize.y), text, guiStyle);
        }
    }


    #endregion // UNTIY_MONOBEHAVIOUR_METHODS
}
