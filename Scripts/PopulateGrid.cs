﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopulateGrid : MonoBehaviour
{
	// This is our prefab object that will be exposed in the inspector
	public GameObject prefab;
	public GameObject MainComments;
	public GameObject SeeComment;

	public SeeCommentClass mySeeCommentClass;

	public Image temoinAudio;

	public Sprite textureSource;
	public Sprite textureNoSource;


	void Start()
	{
		Populate();
	}

	public void Populate()
	{
		foreach (Transform child in this.transform)
		{
		     Destroy(child.gameObject);
		}

		// Create GameObject instance
		GameObject newObj; 
		Debug.Log("Taille commentsOnStep : " + setMenuMaintenance.commentsOnStep.Count);

		foreach(CommentClass comment in setMenuMaintenance.commentsOnStep)
		{
			newObj = (GameObject)Instantiate(prefab, transform);
			newObj.GetComponentInChildren<Text>().text = comment.nameUser + " - " + comment.title;
			Button _button = newObj.GetComponentInChildren<Button> ();
            _button.onClick.AddListener (() => {clicked(comment);});
		}

	}

	//Display the comments when the button is clicked
	public void clicked(CommentClass comment){
		CommentClass.currentComment = comment;
		Debug.Log("Comment clicked : " + CommentClass.currentComment.title);

		mySeeCommentClass.setCommentValue();

		if(mySeeCommentClass.audioURL.Equals(""))
			temoinAudio.sprite = textureNoSource;
		else
			temoinAudio.sprite = textureSource;

		SeeComment.SetActive(true);

     }
}