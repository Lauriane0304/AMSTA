using UnityEngine;
using System.Collections;

// This class is serializable and represents the comment's structure
[System.Serializable] 
public class AMSTAComment {
 
    public string title;
    public string commentary;
    public string videoSource;
    public string audioSource;
}