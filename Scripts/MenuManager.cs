﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


// This class handle the behaviour of the UI for the MainMenu scene
public class MenuManager : MonoBehaviour
{

    public Button myButtonOperationalMode;
    public Button myButtonMROMode;

    public Button myButtonCubeSat;
    public Button myButtonAMSTADrill;
    public Button myButtonRack;
    public Button myButtonBattery;
    public Button myButtonProcedure;
    public Button myButtonLCPRack;
    public Button myButtonBack;
    public Button myButtonGlass;
    public Button myButtonIMUProcedure;
    public Button myButtonIMUTechnicalSheet;
    public Button myButtonAMSTADrillWiringDiagram;
    public Button myButtonCheckProcedure;
    public GameObject backButton;

    public Button yesButton;
    public Button noButton;

    public Button myButtonClose;

    public GameObject myMainMenu;
    public GameObject myMROMenu;
    public GameObject myMRODocs;

    public UnityEngine.UI.Image myImageDocTech;

    public Sprite SpriteWiringDiagram;
    public Sprite SpriteTechnicalSheetIMU;

    public bool isButtonModePress = false;

    public void Start()
    {
        Screen.orientation = ScreenOrientation.LandscapeLeft;

        ActivateMainMenu(true);

        myButtonOperationalMode.onClick.AddListener(TaskOnClick);
        myButtonMROMode.onClick.AddListener(TaskOnClick);

        myButtonCubeSat.onClick.AddListener(TaskOnClick);
        myButtonAMSTADrill.onClick.AddListener(TaskOnClick);
        myButtonBattery.onClick.AddListener(TaskOnClick);
        myButtonProcedure.onClick.AddListener(TaskOnClick);
        myButtonBack.onClick.AddListener(TaskOnClick);
        myButtonGlass.onClick.AddListener(TaskOnClick);
        myButtonIMUProcedure.onClick.AddListener(TaskOnClick);
        myButtonRack.onClick.AddListener(TaskOnClick);
        myButtonLCPRack.onClick.AddListener(TaskOnClick);
        myButtonCheckProcedure.onClick.AddListener(TaskOnClick);
        myButtonIMUTechnicalSheet.onClick.AddListener(TaskOnClick);
        myButtonAMSTADrillWiringDiagram.onClick.AddListener(TaskOnClick);
        myButtonClose.onClick.AddListener(TaskOnClick);
        yesButton.onClick.AddListener(TaskOnClick);
        noButton.onClick.AddListener(TaskOnClick);
        
        HideButtons();

        backButton.SetActive(false);

    }

    void ChangeScene(string sceneName)
    {
        SceneManager.LoadScene (sceneName);
    }

    void ActivateMainMenu(bool active)
    {
        if(active)
        {
            myMROMenu.SetActive(false);
            myMainMenu.SetActive(true);
            myMRODocs.SetActive(false);
        }
        else
        {
            myMROMenu.SetActive(true);
            myMainMenu.SetActive(false);
            myMRODocs.SetActive(false);
        }
    }

    void HideButtons()
    {
        myButtonProcedure.gameObject.SetActive(false);
        myButtonBattery.gameObject.SetActive(false);
        myButtonIMUProcedure.gameObject.SetActive(false);
        myButtonGlass.gameObject.SetActive(false);
        myButtonLCPRack.gameObject.SetActive(false);
        myButtonCheckProcedure.gameObject.SetActive(false);
        myButtonIMUTechnicalSheet.gameObject.SetActive(false);
        myButtonAMSTADrillWiringDiagram.gameObject.SetActive(false);
    }



    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape)) {
            if (isButtonModePress) {
                HideButtons();
                ActivateMainMenu(true);
                isButtonModePress = false;
            }
            else {
                backButton.SetActive(true);
            }
        }
    }


    void TaskOnClick()
    {
        if(EventSystem.current.currentSelectedGameObject.name == "ButtonOperationalMode")
        {
            Debug.Log("Button Operational Mode Pressed !");

            ChangeScene("AMSTATabletteOperationnal");
            isButtonModePress = true;
        }
        
        else if(EventSystem.current.currentSelectedGameObject.name == "ButtonMROMode")
        {
            Debug.Log("Button MRO Mode Pressed !");

            ActivateMainMenu(false);
            isButtonModePress = true;
        }

        else if(EventSystem.current.currentSelectedGameObject.name == "ButtonCubeSat")
        {
            Debug.Log("Button CubeSat Pressed !");

            HideButtons();
            myButtonBattery.gameObject.SetActive(true);
        }

        else if(EventSystem.current.currentSelectedGameObject.name == "ButtonAMSTADrill")
        {
            Debug.Log("Button AMSTA Drill Pressed !");

            HideButtons();
            myButtonGlass.gameObject.SetActive(true);
            myButtonAMSTADrillWiringDiagram.gameObject.SetActive(true);
        }

        else if(EventSystem.current.currentSelectedGameObject.name == "ButtonRack")
        {
            Debug.Log("Button Rack Pressed !");

            HideButtons();
            myButtonLCPRack.gameObject.SetActive(true);
        }

        else if(EventSystem.current.currentSelectedGameObject.name == "ButtonBattery")
        {
            Debug.Log("Button Battery Pressed !");

            myButtonProcedure.gameObject.SetActive(true);
        }

        else if(EventSystem.current.currentSelectedGameObject.name == "ButtonIMUGlass")
        {
            Debug.Log("Button IMU Glass Pressed !");

            myButtonIMUProcedure.gameObject.SetActive(true);
            myButtonIMUTechnicalSheet.gameObject.SetActive(true);
        }

        else if(EventSystem.current.currentSelectedGameObject.name == "ButtonLCPRack")
        {
            Debug.Log("Button IMU Glass Pressed !");

            myButtonCheckProcedure.gameObject.SetActive(true);
        }

        else if(EventSystem.current.currentSelectedGameObject.name == "ButtonProcedure")
        {
            Debug.Log("Button Procedure Pressed !");

            DepotLinks.versionLink = "https://drive.google.com/uc?authuser=0&id=1L3aW0EYNmG4sT-lL8wrnDK2Da-HUu6a6&export=download";
            DepotLinks.mainLink = "https://drive.google.com/uc?authuser=0&id=19J4LRW1WjzeDnbGUmmiWeyAx6J1KJfqT&export=download";

            ChangeScene("SceneEmptyShell");
        }

        else if(EventSystem.current.currentSelectedGameObject.name == "ButtonIMUProcedure")
        {
            Debug.Log("Button IMU Procedure Pressed !");

            DepotLinks.versionLink = "https://drive.google.com/uc?authuser=0&id=1_OPFhqzHZXK0GHl7EZbg9PGLy3rqM4u1&export=download";
            DepotLinks.mainLink = "https://drive.google.com/uc?authuser=0&id=1rq9KoIePXMQFp46CSefLJ5-s-ooL59cv&export=download";

            ChangeScene("CheckListIMU");
        }

        else if(EventSystem.current.currentSelectedGameObject.name == "ButtonRackCheck")
        {
            Debug.Log("Button Rack Check Pressed !");

            DepotLinks.versionLink = "https://drive.google.com/uc?authuser=0&id=1Aq98PtMoA240uWYIrXmll8_BvNn72577&export=download";
            DepotLinks.mainLink = "https://drive.google.com/uc?authuser=0&id=1vOYKlD29GPjK2EAlXTmzrEJg9wJj5Ras&export=download";

            ChangeScene("SceneEmptyShell");
        }

        else if(EventSystem.current.currentSelectedGameObject.name == "ButtonAMSTADrillWiringDiagram")
        {
            Debug.Log("Button Drill Wiring Diagram Pressed !");

            myMROMenu.SetActive(false);
            myMRODocs.SetActive(true);
            myImageDocTech.sprite = SpriteWiringDiagram;
        }

        else if(EventSystem.current.currentSelectedGameObject.name == "ButtonIMUTechnicalSheet")
        {
            Debug.Log("Button IMU Technical Sheet Pressed !");

            myMROMenu.SetActive(false);
            myMRODocs.SetActive(true);
            myImageDocTech.sprite = SpriteTechnicalSheetIMU;
        }

        else if(EventSystem.current.currentSelectedGameObject.name == "ButtonClose")
        {
            Debug.Log("Button IMU Technical Sheet Pressed !");

            myMROMenu.SetActive(true);
            myMRODocs.SetActive(false);
        }

        else if(EventSystem.current.currentSelectedGameObject.name == "ButtonBack")
        {
            Debug.Log("Button Back Pressed !");

            HideButtons();
            ActivateMainMenu(true);
        }

        else if (EventSystem.current.currentSelectedGameObject.name == "YesButton") {
            Application.Quit();
        }
        else if (EventSystem.current.currentSelectedGameObject.name == "NoButton") {
            backButton.SetActive(false);
        } 
    }
}