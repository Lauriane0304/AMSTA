﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SeeCommentClass : MonoBehaviour
{
	public UnityEngine.UI.Text titleComment;
	public UnityEngine.UI.Text NameUserComment;
	public UnityEngine.UI.Text DateComment;
	public UnityEngine.UI.Text CommentaryComment;

	public Image ScreenshotPlane;

	public string videoURL;
	public string audioURL;

	public IMG2Sprite myIMG;



	private Sprite mySprite;

	//Allows to get back the comments already saved
	public void setCommentValue()
	{
		titleComment.text = CommentClass.currentComment.title;
		NameUserComment.text = CommentClass.currentComment.nameUser;
		DateComment.text = CommentClass.currentComment.date;
		CommentaryComment.text = CommentClass.currentComment.commentText;
		videoURL = CommentClass.currentComment.linkVideo;
		audioURL = CommentClass.currentComment.linkAudio;

		Debug.Log(videoURL);
		mySprite = myIMG.LoadNewSprite(videoURL, 100.0f);
		ScreenshotPlane.sprite = mySprite;
	}			
}
