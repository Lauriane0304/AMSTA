﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// This class is serializable and represents the comment's structure of the application
[System.Serializable] 
public class CommentClass{

	// Refers to the current comment from everywhere
	public static CommentClass currentComment;

	public int step;
	public string title;
	public string nameUser;
	public string date;
	public string commentText;
	public string linkVideo;
	public string linkAudio;

	public CommentClass()
	{
        this.step = 0;
	    this.title = "";
		this.nameUser = "";
		this.date = "";
		this.commentText = "";
		this.linkVideo = "";
		this.linkAudio = "";
    }
	
}
