using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;
using Vuforia;
using UnityEngine.SceneManagement;

// This class handle the downloading 
public class MyAssetBundleManager : MonoBehaviour, ITrackableEventHandler 
{
    public GameObject myImageTarget1;
    public GameObject myImageTarget2;
    public GameObject myImageTarget3;
    public GameObject myImageTarget4;
    public GameObject myImageTarget5;
    public GameObject myImageTarget6;
    public GameObject myNoImageTarget;
    public GameObject myTemoinGO;
    public GameObject myCurrentImageTarget;

    public GameObject backButton;

    public Button yesButton;
    public Button noButton;

    public GameObject myCanvasTarget;
    public bool IsNoImageTarget = false;

    public GameObject myListOfTexts;
    public Text myTitle;

    public Text myTextStep1;
    public Text myTextStep2;
    public Text myTextStep3;
    public Text myTextStep4;
    public Text myTextStep5;

    public Text myTextCanvas;
    public Text myTextPannelProc;
    public UnityEngine.UI.Image myImage1;
    public UnityEngine.UI.Image myImage2;

    public Sprite textureGenerateReportToDisplay;
    public Sprite textureNextToDisplay;

    public bool BundleReady = false;
   
    public bool ProblemDownloading = false;
    public bool WarningDownloading = false;
   
    public bool isInfoCanvasDisplayed = false;

    public int totalScene;
    public string commentsFileName;




    private JSONIndex index;
    private GameObject[] myCurrentObjects;
    private Sprite[] mySprites;
   
    private string url;
    private string urlVersion;

    private int lastVersion;
    private int version;

    private Text[] childText;
    private string[] ListOfTasks;

    private WWW www;
    private WWW wwwVersion;
    private UnityWebRequest wwwWeb;
    private AssetBundle bundle;
    private AssetBundle bundleOfVersion;

    private TrackableBehaviour mTrackableBehaviour;

    private bool mGOInstantiated = false;
    private bool finalReached = false;
    private bool firstAccess = false;
    
 
    void Start()
    {
        mTrackableBehaviour = GetComponent<TrackableBehaviour>();
        if (mTrackableBehaviour)
        {
            mTrackableBehaviour.RegisterTrackableEventHandler(this);
        }

        // We write in PlayerPrefs the version of the Bundle Downloaded in cache
        version = PlayerPrefs.GetInt("newVersion",1);

        // Check if cache is empty
        if(PlayerPrefs.GetInt("lastVersion",-1) == -1)
        {
            Debug.Log("First access");
            firstAccess = true;
        }
        else
        {
            lastVersion = PlayerPrefs.GetInt("lastVersion",1);
        }

        // Refresh the links with the static affectation
        url = DepotLinks.mainLink;
        urlVersion = DepotLinks.versionLink;

        backButton.SetActive(false);
    }


    public void WaitingForNetwork() {
        Debug.Log("WaitingForNetwork : enter");

        StartCoroutine(LoadBundle());
    }

    // This method is running in background in order to download the AssetBundle
    public IEnumerator LoadBundle() 
    {
        Debug.Log("Loading Bundle...");
        
        /*
        while (!Caching.ready)
        {
            yield return null;
        }*/

        wwwWeb = UnityWebRequest.GetAssetBundle(DepotLinks.versionLink);
        yield return wwwWeb.SendWebRequest();
 
        if(wwwWeb.isNetworkError || wwwWeb.isHttpError) {
            Debug.Log(wwwWeb.error);

        }
        else {
            bundleOfVersion = DownloadHandlerAssetBundle.GetContent(wwwWeb);
        }

        int bundleVersion;

        // Case when there's no internet connection and there's not Bundle loaded in cache
        if(!bundleOfVersion && firstAccess)
        {
            Debug.Log("Connection Problem !");
            ProblemDownloading = true;
            Invoke("WaitingForNetwork", 1);
            yield break;
        }

        // Case when there's no internet connection and there's a Bundle loaded in cache
        else if(!bundleOfVersion && !firstAccess)
        {
            Debug.Log("Warning, loading bundle in cache");
            WarningDownloading = true;
            Invoke("Wait",3);
            bundleVersion = lastVersion;
        }

        // Download the last version of the bundle online and compare to the version of the Bundle in cache
        else
        {
            ProblemDownloading = false;
            TextAsset myVersion = bundleOfVersion.LoadAsset<TextAsset>("lastVersion.txt");
            Debug.Log("Version of Bundle :" + myVersion.text);
            lastVersion = Convert.ToInt32(myVersion.text);
            bundleVersion = lastVersion;
            PlayerPrefs.SetInt("lastVersion", lastVersion);

            //Close the bundleOfVersion
            bundleOfVersion.Unload(false);
            wwwWeb.Dispose();
        }

        PlayerPrefs.SetInt("newVersion", version+1);

        
 
        //Begin download
        www = WWW.LoadFromCacheOrDownload (DepotLinks.mainLink, bundleVersion);
        yield return www;
 
        //Load the downloaded bundle
        bundle = www.assetBundle;

        if(!bundle)
        {
            Debug.Log("Problem");
            ProblemDownloading = true;
            yield break;
        }

        // Get the JSON file and load it
        TextAsset myIndex = bundle.LoadAsset<TextAsset>("index.json");
        index = JsonUtility.FromJson<JSONIndex>(myIndex.text);

        totalScene = index.scenes.Length;

        commentsFileName = index.commentsFileName;

        // Load the banner sprite of each ImageTarget
        foreach(JSONBanner banner in index.banners)
        {
            Sprite currentSprite;
            AssetBundleRequest bundleRequest;
            
            if (banner.spriteName == "")
                break;
           
            else 
                bundleRequest = bundle.LoadAssetAsync (banner.spriteName, typeof(Sprite));
               
            yield return bundleRequest;
     
            currentSprite = bundleRequest.asset as Sprite;

            switch(banner.targetNumber)
            {
                case 1:
                    myImageTarget1.GetComponent<OnTargetAquired>().textureScanTargetToDisplay = currentSprite;
                    break;
                case 2:
                    myImageTarget2.GetComponent<OnTargetAquired>().textureScanTargetToDisplay = currentSprite;
                    break;
                case 3:
                    myImageTarget3.GetComponent<OnTargetAquired>().textureScanTargetToDisplay = currentSprite;
                    break;
                case 4:
                    myImageTarget4.GetComponent<OnTargetAquired>().textureScanTargetToDisplay = currentSprite;
                    break;
                case 5:
                    myImageTarget5.GetComponent<OnTargetAquired>().textureScanTargetToDisplay = currentSprite;
                    break;
                case 6:
                    myImageTarget6.GetComponent<OnTargetAquired>().textureScanTargetToDisplay = currentSprite;
                    break;
            }
        }
        
        

        BundleReady = true;
        Debug.Log("Bundle Ready !");

        // We instantiate the first scene
        StartCoroutine(Instantiate(1));

        InitList();
        UpdateList(1,1);
    }


    public void Wait() {
        WarningDownloading = false;
    }


    // This method instantiate the different elements of the scene relative to the number of the step
    public IEnumerator Instantiate(int mySceneNumber)
    {
        // Case when the user is at the end of the procedure
        if(mySceneNumber > totalScene)
        {
            CloseAll();

            #if UNITY_EDITOR
                // Application.Quit() does not work in the editor so
                // UnityEditor.EditorApplication.isPlaying need to be set to false to end the game
                SceneManager.LoadScene ("MainScene");
            #else
                SceneManager.LoadScene ("MainScene");
            #endif
            yield break;
        }

        // Case when the user is at the last step of the procedure
        else if(mySceneNumber == totalScene)
        {
            changeButtonFinalTexture();
            finalReached = true;
        }

        if(mySceneNumber<totalScene && finalReached)
            changeButtonNextTexture();

        Debug.Log("Instantiacing scene " + mySceneNumber);

        //Canvas settings
        mySprites = new Sprite[2];
        int numberSprite = 0;
        foreach (JSONScene scene in index.scenes) 
        {
            if(scene.sceneNumber == mySceneNumber)
            {
                foreach(JSONSprite sprite in scene.sprites)
                {
                    AssetBundleRequest bundleRequest;
                    if (sprite.name == "") {
                        break;
                    }
                    else {
                        //Load an asset from the loaded bundle
                        bundleRequest = bundle.LoadAssetAsync (sprite.name, typeof(Sprite));
                        Debug.Log("Image name :" + sprite.name);
                    }
                    yield return bundleRequest;
             
                    //get object
                    if(numberSprite>1)
                        break;
                    mySprites[numberSprite]  = bundleRequest.asset as Sprite;
                    numberSprite++;
                }

                myTextCanvas.text = scene.name;
                myTextPannelProc.text = scene.name;

                if(scene.sprites.Length == 0)
                {
                    Color c = myImage1.color;
                    c.a = 0;
                    myImage1.color = c;
                    myImage2.color = c;
                }

                else if(scene.sprites.Length == 1)
                {
                    Color c = myImage2.color;
                    c.a = 0;
                    myImage2.color = c;

                    c.a = 100;
                    myImage1.color = c;
                    myImage1.sprite = mySprites[0];
                }

                else if(scene.sprites.Length == 2)
                {
                    Color c = myImage1.color;
                    c.a = 100;
                    myImage1.color = c;
                    myImage2.color = c;
                    myImage1.sprite = mySprites[0];
                    myImage2.sprite = mySprites[1];
                }

                else
                    break;

            }
        }
        
        // Refresh each textStep
        myTextStep1.text = "Step : " + mySceneNumber.ToString() + " / " + totalScene.ToString();
        myTextStep2.text = "Step : " + mySceneNumber.ToString() + " / " + totalScene.ToString();
        myTextStep3.text = "Step : " + mySceneNumber.ToString() + " / " + totalScene.ToString();
        myTextStep4.text = "Step : " + mySceneNumber.ToString() + " / " + totalScene.ToString();
        myTextStep5.text = "Step : " + mySceneNumber.ToString() + " / " + totalScene.ToString();

        //Select the right target
        foreach (JSONScene scene in index.scenes) 
        {
            if(scene.sceneNumber == mySceneNumber)
            {
                myImageTarget1.GetComponent<OnTargetAquired>().TargetActivated = false;
                myImageTarget2.GetComponent<OnTargetAquired>().TargetActivated = false;
                myImageTarget3.GetComponent<OnTargetAquired>().TargetActivated = false;
                myImageTarget4.GetComponent<OnTargetAquired>().TargetActivated = false;
                myImageTarget5.GetComponent<OnTargetAquired>().TargetActivated = false;
                myImageTarget6.GetComponent<OnTargetAquired>().TargetActivated = false;

                myCanvasTarget.SetActive(true);
                IsNoImageTarget = false;


                switch(scene.target)
                {
                    case 1 :
                        myCurrentImageTarget = myImageTarget1;
                        myCurrentImageTarget.name = "MarqueurAMSTA1";
                        myImageTarget1.GetComponent<OnTargetAquired>().TargetActivated = true;
                        break;
                    case 2 :
                        myCurrentImageTarget = myImageTarget2;
                        myCurrentImageTarget.name = "MarqueurAMSTA2";
                        myImageTarget2.GetComponent<OnTargetAquired>().TargetActivated = true;
                        break;
                    case 3 :
                        myCurrentImageTarget = myImageTarget3;
                        myCurrentImageTarget.name = "MarqueurAMSTA3";
                        myImageTarget3.GetComponent<OnTargetAquired>().TargetActivated = true;
                        break;
                    case 4 :
                        myCurrentImageTarget = myImageTarget4;
                        myCurrentImageTarget.name = "MarqueurAMSTA4";
                        myImageTarget4.GetComponent<OnTargetAquired>().TargetActivated = true;
                        break;
                    case 5 :
                        myCurrentImageTarget = myImageTarget5;
                        myCurrentImageTarget.name = "MarqueurAMSTA5";
                        myImageTarget5.GetComponent<OnTargetAquired>().TargetActivated = true;
                        break;
                    case 6 :
                        myCurrentImageTarget = myImageTarget6;
                        myCurrentImageTarget.name = "MarqueurAMSTA6";
                        myImageTarget6.GetComponent<OnTargetAquired>().TargetActivated = true;
                        break;
                    default :
                        myCurrentImageTarget = myNoImageTarget;
                        myCanvasTarget.SetActive(false);
                        IsNoImageTarget = true;
                        break;
                }
            }
        }

        activateMyGameObject(false);
        

        //Instantiate GameObjetcs
        int GONumber = 0;
        foreach (JSONPrefab prefab in index.prefabs) 
        {
            if(prefab.sceneNumber == mySceneNumber)
                GONumber++;
        }

        myCurrentObjects = new GameObject[GONumber];
        
        int i = 0;
        
        foreach (JSONPrefab prefab in index.prefabs) 
        {
            if(prefab.sceneNumber == mySceneNumber)
            {
                AssetBundleRequest bundleRequest;
                if (prefab.name == "") {
                    break;
                }
                else {
                    //Load an asset from the loaded bundle
                    bundleRequest = bundle.LoadAssetAsync (prefab.name, typeof(GameObject));
                    Debug.Log(prefab.name);
                }
                yield return bundleRequest;
         
                //get object
                GameObject obj = bundleRequest.asset as GameObject;
                
                if(i >= GONumber)
                {
                    yield break;
                }
                
                myCurrentObjects[i] = Instantiate(obj) as GameObject;

                //select the good target
                switch(prefab.target)
                {
                    case 1:
                        myCurrentObjects[i].transform.parent = myImageTarget1.transform.GetChild(0).transform;
                        break;
                    case 2:
                        myCurrentObjects[i].transform.parent = myImageTarget2.transform.GetChild(0).transform;
                        break;
                    case 3:
                        myCurrentObjects[i].transform.parent = myImageTarget3.transform.GetChild(0).transform;
                        break;
                    case 4:
                        myCurrentObjects[i].transform.parent = myImageTarget4.transform.GetChild(0).transform;
                        break;
                    case 5:
                        myCurrentObjects[i].transform.parent = myImageTarget5.transform.GetChild(0).transform;
                        break;
                    case 6:
                        myCurrentObjects[i].transform.parent = myImageTarget6.transform.GetChild(0).transform;
                        break;
                    // Mini système
                    case 7:
                        myCurrentObjects[i].transform.parent = myTemoinGO.transform;
                        break;
                    // Temoin du mini systeme
                    case 8:
                        myCurrentObjects[i].transform.parent = myTemoinGO.transform;
                        myCurrentObjects[i].transform.GetChild(0).gameObject.AddComponent<AnimTemoin>();
                        break;
                }
                       
                myCurrentObjects[i].transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
                myCurrentObjects[i].transform.localPosition = new Vector3(0.0f, 0.0f, 0.0f);
                Vector3 temp = myCurrentObjects[i].transform.localRotation.eulerAngles;
                temp = new Vector3(0.0f, 0.0f, 0.0f);
                myCurrentObjects[i].transform.localRotation = Quaternion.Euler(temp);

                Debug.Log("Object Instantiated");
                i++;
            }
        }
        
        mGOInstantiated = true;
        activateMyGameObject(false);
    }

    public void DestroyGameObjects() 
    {
        if(mGOInstantiated)
        {
            for(int i=0; i<myCurrentObjects.Length; i++)
            {
                Destroy(myCurrentObjects[i]);
            }
        
            mGOInstantiated = false;
            Debug.Log("GO All clear !");
        }
    }
  
    public void CloseAll()
    {
        DestroyGameObjects();
        bundle.Unload(false);
        www.Dispose();
        BundleReady = false;
        Debug.Log("All is closed !");
    }

    // This method inits the strings for the list of steps
    private void InitList()
    {
        myTitle.text = index.title;

        ListOfTasks = new string[totalScene];

        int i = 0;
        foreach(JSONScene scene in index.scenes)
        {
            ListOfTasks[i] = (i+1).ToString() + " - " + scene.name;
            i++;
        }

        SetList(1);
    }

    // This method instantiate the strings of the step list
    public void SetList(int pageList)
    {
        int i = 14*(pageList -1);
        childText = myListOfTexts.GetComponentsInChildren<Text>();

        foreach(Text child in childText)
            child.text = "";
        foreach(Text child in childText)
        {
            if(i>=totalScene)
                break;
            child.text = ListOfTasks[i];
            i++;
        }
    }

    // This method update the strings of the step list after each push on next or previous
    public void UpdateList(int sceneNumber, int pageList)
    {
        int i = 14*(pageList-1) + 1;
        foreach (Text child in childText)
        {
            if(i <= sceneNumber)
            {
                child.fontStyle = FontStyle.Bold;
                child.color = Color.blue;
            }
            else
            {
                child.fontStyle = FontStyle.Normal;
                child.color = Color.black;
            }
            i++;            
        }
    }

    // This method activate or disactivate the gameobject during the "info mode" or between each load of steps
    public void activateMyGameObject (bool active){

        int childcount = myCurrentImageTarget.transform.GetChild(0).childCount;
        Debug.Log("Childcount " + childcount);

        for (int i = 0; i < childcount; i++) {
            myCurrentImageTarget.transform.GetChild(0).transform.GetChild(i).gameObject.SetActive(active);
        }
        Debug.Log("All my gameobjects are activated or disactivated");
    }

    public void changeButtonFinalTexture()
    {
        GameObject.Find("ARCamera").GetComponent<setMenuMaintenance>().myMainButtonNext.GetComponent<UnityEngine.UI.Image>().sprite = textureGenerateReportToDisplay;
    }

    public void changeButtonNextTexture()
    {
        GameObject.Find("ARCamera").GetComponent<setMenuMaintenance>().myMainButtonNext.GetComponent<UnityEngine.UI.Image>().sprite = textureNextToDisplay;
    }
     
    public void OnTrackableStateChanged(
        TrackableBehaviour.Status previousStatus,
        TrackableBehaviour.Status newStatus)
    {
    }
}