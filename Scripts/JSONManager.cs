﻿using UnityEngine;
using System.Collections;

using System;
using System.IO;
using System.Net.Sockets;
using System.Net;
using System.Text;

// This class handle the wifi connexion between the device and the arduino.
public class JSONManager : MonoBehaviour {
	public Int32 Port;							// Port
	public string Host;							// Host IP or domain.
	public GameObject myArrow;

	IPAddress[] ips;

	private string myText = "";					// Line received from the TCP stream
	private bool socketReady = false;
	
	UdpClient mySocket;
	
	// Use this for initialization
	void Start () {

		setupSocket();
	}
	
	// Update is called once per frame used to receive TCP Socket-IO data.
	void Update () {

		string receivedText = readSocket();
		
		if (receivedText != "") 
		{
			myText = receivedText;
			
			//myText = "{"sensors":[{"name":"Nanolike1","data":[{"title":"BrownRice","value":"1.11"}]},{"name":"PST1","data":[{"title":"White Rice","value":"2.22"}]},{"name":"Hum","data":[{"title":"Brown Rice","value":"3.33"}]},{"name":"MPU2650","data":[{"title":"White Rice1","value":"4.44"},{"title":"White Rice2","value":"5.44"},{"title":"White Rice3","value":"6.44"},{"title":"White Rice4","value":"7.44"}]}]}";

			JSONSensors sensors = JsonUtility.FromJson<JSONSensors>(myText);

			foreach (JSONSensor sensor in sensors.sensors) {
				if(string.Equals(sensor.name, "")) {
                    break;
                }

                string str_values = "";

				if(string.Equals(sensor.name, "MPU6050") || string.Equals(sensor.name, "MPU2650"))
                {
                	str_values = "Sensor : IMU\n\n";
                }



                foreach (JSONSensorData data in sensor.data)
                {
                	str_values += (data.title + " :     " + data.value/100 + "\n");
                }

                if(string.Equals(sensor.name, "MPU6050") || string.Equals(sensor.name, "MPU2650"))
                {
                	myArrow.GetComponent<SensorInfoBehaviour>().text = str_values;
                }


			}

			
			
		}
	}


	// Ensure that the socket closes on application close
	void OnApplicationQuit(){	
		closeSocket();		
	}

	// Opens the network TCP socket and starts read and write streams.
	public void setupSocket() {
		try {
			Debug.Log("Socket setting up !!!");
			mySocket = new UdpClient(Port);
			Debug.Log("Socket Ready !!!");
			socketReady = true;
		} catch (Exception e) {
			print(e.ToString());
		}
	}

	// Reads a line of text from the TCP stream if available
	public String readSocket() {

		if (!socketReady) {
			return "";
		}

		IPEndPoint groupEP = new IPEndPoint(IPAddress.Parse(Host),Port);

		try{
			StartCoroutine(Wait());
            if (mySocket.Available > 0)
            {
	            Byte[] receiveBytes = mySocket.Receive(ref groupEP);
	            return Encoding.UTF8.GetString(receiveBytes);
            } else {
                //Debug.Log("NO DATA AVAILABLE");
            }
        }
        catch ( Exception e ){
            Debug.Log(e.ToString());
        }
		return "";
	}

	IEnumerator Wait()
    {
        yield return new WaitForSeconds(1000);
    }

	// Closes TCP connection and streams.
	public void closeSocket() {
		if (!socketReady) {
			return;
		}

		mySocket.Close();
		socketReady = false;
	}

}