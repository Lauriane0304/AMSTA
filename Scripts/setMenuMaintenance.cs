using UnityEngine;
using System.Collections;
using System;
using Vuforia;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using RockVR.Video;
using System.Collections.Generic;

// This class manage the graphic interface of the scene particularly the canvas (flas, info, next buttons, ...)
public class setMenuMaintenance : MonoBehaviour
{
    //Variable of AssetBundle Manager type allowing to use AssetBundles more streamlined.
    public MyAssetBundleManager myABM;

    //Boolean to show the state of the next and previous buttons and of the canvas
    public bool mPreviousPushed = false;
    public bool mNextPushed = false;
    public bool isCanvasActivate = false;

    //Buttons
    public UnityEngine.UI.Button myButtonPannel;
    public UnityEngine.UI.Button myMainButtonFlash;
    public UnityEngine.UI.Button myMainButtonInfo;
    public UnityEngine.UI.Button myMainButtonPrevious;
    public UnityEngine.UI.Button myMainButtonNext;
    public UnityEngine.UI.Button myMainButtonPlay;

    public UnityEngine.UI.Button myButtonBack;
    public UnityEngine.UI.Button myButtonSeeAll;
    public UnityEngine.UI.Button myButtonPrevious;
    public UnityEngine.UI.Button myButtonNext;

    public UnityEngine.UI.Button myButtonComments;
    public UnityEngine.UI.Button myButtonBackComments;
    public UnityEngine.UI.Button myButtonAdd;
    public UnityEngine.UI.Button myButtonBackAddComments;
    public UnityEngine.UI.Button myButtonSave;
    public UnityEngine.UI.Button myButtonBackComment;
    public UnityEngine.UI.Button myButtonDelete;
    public UnityEngine.UI.Button myButtonListenAudio;
    public UnityEngine.UI.Button myButtonScreenshot;
    public UnityEngine.UI.Button myButtonRecordingAudio;
    public UnityEngine.UI.Button myButtonTakePhoto;

    public GameObject myVideo;
    public GameObject myVideoAvancee;
    public GameObject myMainDisplay;
    public GameObject myInfo;
    public GameObject myInfoDisplay;
    public GameObject mySummary;
    public GameObject myMainCommentsDisplay;
    public GameObject myAddCommentDisplay;
    public GameObject mySeeCommentDisplay;
    public GameObject myCanvasTarget;
    public GameObject myPannel;

    public UnityEngine.UI.Image myImagePannel;
    public UnityEngine.UI.Image sourcePhoto;
    public UnityEngine.UI.Image sourceAudio;

    public UnityEngine.UI.InputField textTitleComment;
    public UnityEngine.UI.InputField textCommentComment;
    public UnityEngine.UI.InputField textDateComment;
    public UnityEngine.UI.InputField textNameUserComment;

    //Sprites are 2D graphic objects used for characters, props, projectiles and other elments of 2D gameplay
    public Sprite openPannelSprite;
    public Sprite closePannelSprite;

    public Sprite textureFlashOnToDisplay;
    public Sprite textureFlashOffToDisplay;
    public Sprite texturePreviousToDisplay;
    public Sprite textureNextToDisplay;
    public Sprite textureBackToDisplay;
    public Sprite textureSeeAllToDisplay;
    public Sprite texturePlay;
    public Sprite texturePause;
    public Sprite textureSource;
    public Sprite textureNoSource;

    public Sprite textureComments0;
    public Sprite textureComments1;
    public Sprite textureComments2;
    public Sprite textureComments3;
    public Sprite textureComments4;
    public Sprite textureComments5;
    public Sprite textureComments6;
    public Sprite textureComments7;
    public Sprite textureComments8;
    public Sprite textureComments9;
    public Sprite textureComments9p;

    public UnityEngine.UI.Text FramerateText;

    public VideoCaptureCtrl myVideoCaptureCtrl;

    public PopulateGrid myContentComment;

    public static List<CommentClass> commentsOnStep = new List<CommentClass>();






    private bool isAudioRecorded = false;
    private bool isPhotoTaken = false;
    private bool isOpen = false;
    private bool mFlashEnabled = false;
    private bool isVideoReady = false;
    private bool isPaused = true;
    private bool listActivated = false;

    private string myScreenshotName = "";

    private int currentPageList;
    private int totalPageList;
    private int scene = 1;
    private int myFramerate = 0;

    private float seekposition;


    // Contains the currently set auto focus mode.
    private CameraDevice.FocusMode mFocusMode;
    

    /**Start is called on the frame when a script is enabled 
    /* just before any of the Update methods is called the first time.
    */
    public void Start()
    {
        Screen.orientation = ScreenOrientation.LandscapeLeft;
        print(mFocusMode);

        //Add buttons to the listener TaskOnClick
        myMainButtonFlash.onClick.AddListener(TaskOnClick);
        myMainButtonInfo.onClick.AddListener(TaskOnClick);
        myMainButtonPrevious.onClick.AddListener(TaskOnClick);
        myMainButtonNext.onClick.AddListener(TaskOnClick);
        myMainButtonPlay.onClick.AddListener(TaskOnClick);
        myButtonPrevious.onClick.AddListener(TaskOnClick);
        myButtonNext.onClick.AddListener(TaskOnClick);
        myButtonBack.onClick.AddListener(TaskOnClick);
        myButtonSeeAll.onClick.AddListener(TaskOnClick);

        myButtonComments.onClick.AddListener(TaskOnClick);
        myButtonBackComments.onClick.AddListener(TaskOnClick);
        myButtonAdd.onClick.AddListener(TaskOnClick);
        myButtonBackAddComments.onClick.AddListener(TaskOnClick);
        myButtonSave.onClick.AddListener(TaskOnClick);
        myButtonBackComment.onClick.AddListener(TaskOnClick);
        myButtonDelete.onClick.AddListener(TaskOnClick);
        myButtonListenAudio.onClick.AddListener(TaskOnClick);
        myButtonScreenshot.onClick.AddListener(TaskOnClick);
        myButtonRecordingAudio.onClick.AddListener(TaskOnClick);

        myButtonTakePhoto.onClick.AddListener(TaskOnClick);

        //Add buttons to the listener Click
        myButtonPannel.onClick.AddListener(Click);


        ActivateMainDisplay(true);
        ActivateListOfTasks(false);

        myAddCommentDisplay.SetActive(false);
        mySeeCommentDisplay.SetActive(false);

        //Activate or disactivate gameobjects
        myButtonSeeAll.gameObject.SetActive(true);
        myButtonBack.gameObject.SetActive(true);
        myButtonNext.gameObject.SetActive(false);
        myButtonPrevious.gameObject.SetActive(false);


        currentPageList = 1;

        //
        StartCoroutine(myABM.LoadBundle());

        //2s delay, repeat every 2s
        InvokeRepeating("DoFocus", 2f, 2f);

        //1s delay, repeat every 1s
        InvokeRepeating("Framerate", 1f, 1f); 


        //Manage comments on this step
        SaveLoadComments.fileName = myABM.commentsFileName;
        SaveLoadComments.Load();
        RefreshComments(); 
    }

    /** Configure the focus mode of the camera
    */
    void DoFocus() 
    {
        if (CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_TRIGGERAUTO))
            mFocusMode = CameraDevice.FocusMode.FOCUS_MODE_TRIGGERAUTO;
    }

    /**
    */
    IEnumerator WaitForScreenshot(int seconds)
    {
        yield return new WaitForSeconds(seconds);
        myButtonTakePhoto.gameObject.SetActive(false);

        ActivateMainComments(true);
        myAddCommentDisplay.SetActive(true);

        myButtonScreenshot.interactable = !isPhotoTaken;
        myButtonSave.interactable = true;
        myButtonRecordingAudio.interactable = !isAudioRecorded;

        sourcePhoto.sprite = textureSource;
    }

    /**
    */
    void Framerate()
    {
        FramerateText.text = "Framerate : " + myFramerate;
        myFramerate = 0;
    }

    /**Activate main display
    */
    void ActivateMainDisplay(bool active)
    {
        if(active)
        {
            myABM.isInfoCanvasDisplayed = false;
            myInfo.SetActive(false);
            myMainDisplay.SetActive(true);
            myABM.activateMyGameObject(true);
            if(!myABM.IsNoImageTarget)
                myCanvasTarget.SetActive(true);
        }
        else
        {
            myABM.isInfoCanvasDisplayed = true;
            myMainDisplay.SetActive(false);
            myCanvasTarget.SetActive(false);
            myInfo.SetActive(true);
            myABM.activateMyGameObject(false);
        }
    }

    /**Activate main comments
    */
    void ActivateMainComments(bool active)
    {
        if(active)
        {
            myMainDisplay.SetActive(false);
            myCanvasTarget.SetActive(false);
            myMainCommentsDisplay.SetActive(true);
        }
        else
        {
            myMainCommentsDisplay.SetActive(false);
            myMainDisplay.SetActive(true);
            myCanvasTarget.SetActive(true);
        }
    }

    /**Activate list of tasks
    */
    void ActivateListOfTasks(bool active)
    {
        if(active)
        {
            myInfoDisplay.SetActive(false);
            mySummary.SetActive(true);
        }
        else
        {
            mySummary.SetActive(false);
            myInfoDisplay.SetActive(true);
        }
    }

    void RefreshComments()
    {
        //Manage Comments
        int numberComments = 0;
        commentsOnStep.Clear();

        foreach(CommentClass comment in SaveLoadComments.savedComments)
        {
            if(comment.step == scene)
            {
                numberComments++;
                commentsOnStep.Add(comment);
            }
        }
        //Choose the right texture
        switch(numberComments)
        {
            case 0:
                myButtonComments.GetComponent<UnityEngine.UI.Image>().sprite = textureComments0;
                break;
            case 1:
                myButtonComments.GetComponent<UnityEngine.UI.Image>().sprite = textureComments1;
                break;
            case 2:
                myButtonComments.GetComponent<UnityEngine.UI.Image>().sprite = textureComments2;
                break;
            case 3:
                myButtonComments.GetComponent<UnityEngine.UI.Image>().sprite = textureComments3;
                break;
            case 4:
                myButtonComments.GetComponent<UnityEngine.UI.Image>().sprite = textureComments4;
                break;
            case 5:
                myButtonComments.GetComponent<UnityEngine.UI.Image>().sprite = textureComments5;
                break;
            case 6:
                myButtonComments.GetComponent<UnityEngine.UI.Image>().sprite = textureComments6;
                break;
            case 7:
                myButtonComments.GetComponent<UnityEngine.UI.Image>().sprite = textureComments7;
                break;
            case 8:
                myButtonComments.GetComponent<UnityEngine.UI.Image>().sprite = textureComments8;
                break;
            case 9:
                myButtonComments.GetComponent<UnityEngine.UI.Image>().sprite = textureComments9;
                break;
            default:
                myButtonComments.GetComponent<UnityEngine.UI.Image>().sprite = textureComments9p;
                break;
                
        }
        myContentComment.Populate();
    }

    public void OnApplicationPause(bool pause)
    {
        // Upon resuming reactivate the torch if it was active before pausing:
        if (!pause)
        {
            if (mFlashEnabled)
            {
                mFlashEnabled = CameraDevice.Instance.SetFlashTorchMode(true);
            }
        }
    }

    void Update()
    {
        myFramerate++;

        if(scene != 1 && !listActivated)
            myMainButtonPrevious.gameObject.SetActive(true);
        else if(scene==1 && !listActivated)
            myMainButtonPrevious.gameObject.SetActive(false);

        isVideoReady = false;
        myVideo.gameObject.SetActive(false);
        myVideoAvancee.gameObject.SetActive(false);
        myMainButtonPlay.gameObject.SetActive(false);

        //If there is no connection, buttons can't be clicked
        if (myABM.ProblemDownloading)
        {   
            myMainButtonFlash.interactable = false;
            myMainButtonInfo.interactable = false;
            myMainButtonPrevious.interactable = false;
            myMainButtonNext.interactable = false;
            myMainButtonPlay.interactable = false;
            myButtonComments.interactable = false;
            myButtonPannel.interactable = false;
        }

    }

    /**Listener for the pannel button
    */
    void Click() {
        if(!isOpen)
        {
            myImagePannel.sprite = closePannelSprite;
            Vector3 temp = new Vector3(900.0f,0,0);
            myPannel.transform.localPosition += temp;
            isOpen = !isOpen;
        }
        else
        {
            myImagePannel.sprite = openPannelSprite;
            Vector3 temp = new Vector3(-900.0f,0,0);
            myPannel.transform.localPosition += temp;
            isOpen = !isOpen;
        }
    }

    /**Listener for all the screen buttons
    */
    void TaskOnClick()
    {
        if(EventSystem.current.currentSelectedGameObject.name == "MainButtonFlash")
        {
            Debug.Log("Button Main Flash Pressed !");

            if (!mFlashEnabled)
            {
                // Turn on flash if it is currently disabled.
                myMainButtonFlash.GetComponent<UnityEngine.UI.Image>().sprite = textureFlashOnToDisplay;
                CameraDevice.Instance.SetFlashTorchMode(true);
                mFlashEnabled = true;
            }
            else
            {
                // Turn off flash if it is currently enabled.
                myMainButtonFlash.GetComponent<UnityEngine.UI.Image>().sprite = textureFlashOffToDisplay;
                CameraDevice.Instance.SetFlashTorchMode(false);
                mFlashEnabled = false;
            }

        }

        else if(EventSystem.current.currentSelectedGameObject.name == "MainButtonInfo")
        {
            Debug.Log("Button Main Info Pressed !");

            ActivateMainDisplay(false);
            if(!listActivated)
                ActivateListOfTasks(false);
            else
                ActivateListOfTasks(true);
        }

        else if(EventSystem.current.currentSelectedGameObject.name == "MainButtonPrevious")
        {
            Debug.Log("Button Main Previous Pressed !");

            if(!isPaused)
            {
                VideoPlaybackBehaviour video= myVideo.GetComponent<VideoPlaybackBehaviour>();
                video.VideoPlayer.Pause();
                isPaused = true;
            }

            mPreviousPushed = true;
            myABM.DestroyGameObjects();
            if(myABM.BundleReady)
                StartCoroutine(myABM.Instantiate(--scene));
            myABM.UpdateList(scene, currentPageList);

            RefreshComments();

            // Handle Video Rack
            if(scene==1)
            {
                myVideoAvancee.GetComponent<VideoPlaybackBehaviour>().m_path = "VideoRack1.mp4";
                myVideo.GetComponent<VideoPlaybackBehaviour>().m_path = "VideoRack1.mp4";
            }
            else if (scene==7)
            {
                myVideoAvancee.GetComponent<VideoPlaybackBehaviour>().m_path = "VideoRack2.mp4";
                myVideo.GetComponent<VideoPlaybackBehaviour>().m_path = "VideoRack2.mp4";
            }
        }

        else if(EventSystem.current.currentSelectedGameObject.name == "MainButtonNext")
        {
            Debug.Log("Button Main Next Pressed !");

            if(!isPaused)
            {
                VideoPlaybackBehaviour video= myVideo.GetComponent<VideoPlaybackBehaviour>();
                video.VideoPlayer.Pause();
                isPaused = true;
            }

            mNextPushed = true;
            myABM.DestroyGameObjects();
            if(myABM.BundleReady)
                StartCoroutine(myABM.Instantiate(++scene));
            myABM.UpdateList(scene, currentPageList); 

            RefreshComments();

            // Handle Video Rack
            if(scene==1)
            {
                myVideoAvancee.GetComponent<VideoPlaybackBehaviour>().m_path = "VideoRack1.mp4";
                myVideo.GetComponent<VideoPlaybackBehaviour>().m_path = "VideoRack1.mp4";
            }
            else if (scene==7)
            {
                myVideoAvancee.GetComponent<VideoPlaybackBehaviour>().m_path = "VideoRack2.mp4";
                myVideo.GetComponent<VideoPlaybackBehaviour>().m_path = "VideoRack2.mp4";
            }
        }

        else if(EventSystem.current.currentSelectedGameObject.name == "MainButtonPlay")
        {
            Debug.Log("Button Main Pause Pressed !");

            // Get currently playing video
            VideoPlaybackBehaviour video= myVideo.GetComponent<VideoPlaybackBehaviour>();

            if (video != null)
            {
                if (video.VideoPlayer.IsPlayableOnTexture())
                {
                    // This video is playable on a texture, toggle playing/paused
                    VideoPlayerHelper.MediaState state = video.VideoPlayer.GetStatus();

                    if (state == VideoPlayerHelper.MediaState.PAUSED || state == VideoPlayerHelper.MediaState.READY || state == VideoPlayerHelper.MediaState.STOPPED)
                    {
                        // Play this video on texture where it left off
                        video.VideoPlayer.Play(false, video.VideoPlayer.GetCurrentPosition());
                        myMainButtonPlay.GetComponent<UnityEngine.UI.Image>().sprite = texturePause;
                        isPaused = false;
                    }

                    if (state == VideoPlayerHelper.MediaState.REACHED_END)
                    {
                        // Play this video from the beginning
                        video.VideoPlayer.Play(false, 0);
                        myMainButtonPlay.GetComponent<UnityEngine.UI.Image>().sprite = texturePause;
                        isPaused = false;
                    }

                    if (state == VideoPlayerHelper.MediaState.PLAYING)
                    {
                        // Video is already playing, pause it
                        video.VideoPlayer.Pause();
                        myMainButtonPlay.GetComponent<UnityEngine.UI.Image>().sprite = texturePlay;
                        isPaused = true;
                    }
                }

                else
                {
                    // Display the busy icon
                    video.ShowBusyIcon();
                }
            }
        }

        else if(EventSystem.current.currentSelectedGameObject.name == "ButtonBack")
        {
            ActivateMainDisplay(true);
        }

        else if(EventSystem.current.currentSelectedGameObject.name == "ButtonSeeAll")
        {
            Debug.Log("Button Back Pressed !");

            if(!listActivated)
            {
                myButtonBack.gameObject.SetActive(false);
                totalPageList = (myABM.totalScene / 14) + 1;

                ActivateListOfTasks(true);
                
                myButtonSeeAll.GetComponent<UnityEngine.UI.Image>().sprite = textureBackToDisplay;
                listActivated = true;

                if(currentPageList>1)
                    myButtonPrevious.gameObject.SetActive(true);
                else
                    myButtonPrevious.gameObject.SetActive(false);
                if(currentPageList==totalPageList)
                    myButtonNext.gameObject.SetActive(false);
                else
                    myButtonNext.gameObject.SetActive(true);

            }
            else
            {
                myButtonBack.gameObject.SetActive(true);
                ActivateListOfTasks(false); 
                myButtonSeeAll.GetComponent<UnityEngine.UI.Image>().sprite = textureSeeAllToDisplay;
                listActivated = false;
            }

            
        }

        else if(EventSystem.current.currentSelectedGameObject.name == "ButtonPrevious")
        {
            if(!listActivated)
                return;

            currentPageList--;
            if(currentPageList==1)
                myButtonPrevious.gameObject.SetActive(false);
                
            if(currentPageList<totalPageList)
                myButtonNext.gameObject.SetActive(true);

            myABM.UpdateList(scene,currentPageList);
            myABM.SetList(currentPageList);
        }

        else if(EventSystem.current.currentSelectedGameObject.name == "ButtonNext")
        {
            if(!listActivated)
                return;

            currentPageList++;
            if(currentPageList>1)
                myButtonPrevious.gameObject.SetActive(true);

            if(currentPageList==totalPageList)
                myButtonNext.gameObject.SetActive(false);

            myABM.UpdateList(scene,currentPageList);
            myABM.SetList(currentPageList);
        }

        else if(EventSystem.current.currentSelectedGameObject.name == "CommentButton")
        {
            ActivateMainComments(true);
        }

        else if(EventSystem.current.currentSelectedGameObject.name == "ButtonBackComments")
        {
            ActivateMainComments(false);
        }

        else if(EventSystem.current.currentSelectedGameObject.name == "ButtonAdd")
        {
            myAddCommentDisplay.SetActive(true);
            CommentClass.currentComment = new CommentClass();
        }

        else if(EventSystem.current.currentSelectedGameObject.name == "ButtonBackAddComment")
        {
            myAddCommentDisplay.SetActive(false);
        }

        else if(EventSystem.current.currentSelectedGameObject.name == "ButtonScreenshot")
        {
            myAddCommentDisplay.SetActive(false);
            ActivateMainComments(false);
            myButtonTakePhoto.gameObject.SetActive(true);

            myButtonSave.interactable = false;
            myButtonScreenshot.interactable = false;
            myButtonRecordingAudio.interactable = false;
        }

        else if(EventSystem.current.currentSelectedGameObject.name == "ButtonTakePhoto")
        {
            myScreenshotName = "Screenshot-" + System.DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss") + ".png";
            ScreenCapture.CaptureScreenshot(myScreenshotName);
            Debug.Log("Chemin sur android :" + myScreenshotName);
            isPhotoTaken = true;

            StartCoroutine(WaitForScreenshot(1));
        }

        else if(EventSystem.current.currentSelectedGameObject.name == "ButtonSave")
        {
            CommentClass.currentComment.step = scene;
            CommentClass.currentComment.title = textTitleComment.text;
            CommentClass.currentComment.date = textDateComment.text;
            CommentClass.currentComment.nameUser = textNameUserComment.text;
            CommentClass.currentComment.commentText = textCommentComment.text;

            if(isPhotoTaken)
            {
                CommentClass.currentComment.linkVideo = Application.persistentDataPath + "/" + myScreenshotName;
                Debug.Log(myScreenshotName);
            }
            else
                 CommentClass.currentComment.linkVideo = "";

            if(isAudioRecorded)
                CommentClass.currentComment.linkAudio = "";
            else
                CommentClass.currentComment.linkAudio = "";

            SaveLoadComments.Save();
            SaveLoadComments.Load();

            RefreshComments();

            isPhotoTaken = false;
            isAudioRecorded = false;

            myButtonScreenshot.interactable = !isPhotoTaken;
            myButtonRecordingAudio.interactable = !isAudioRecorded;

            sourcePhoto.sprite = textureNoSource;

            myAddCommentDisplay.SetActive(false);

            // Clear the text area
            textTitleComment.Select();
            textDateComment.Select();
            textNameUserComment.Select();
            textCommentComment.Select();
            textTitleComment.text = "";
            textDateComment.text = "";
            textNameUserComment.text = "";
            textCommentComment.text = "";

            Debug.Log("Path of photo : " + myScreenshotName);

        }

        else if(EventSystem.current.currentSelectedGameObject.name == "ButtonBackComment")
        {
            mySeeCommentDisplay.SetActive(false);
        }

        else if(EventSystem.current.currentSelectedGameObject.name == "ButtonDelete")
        {
            SaveLoadComments.Delete();
            RefreshComments();
            mySeeCommentDisplay.SetActive(false);
        }
    }
}